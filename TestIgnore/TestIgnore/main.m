//
//  main.m
//  TestIgnore
//
//  Created by 魏信洋 on 2017/7/17.
//  Copyright © 2017年 林晓斌. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
